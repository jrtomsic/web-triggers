# Web Triggers

This app will trigger IoT endpoints such as IFTTT and AutoRemote or Join for Tasker. There are seven different inputs that can each do a different web action: four direction swipes, screen tap, and the two buttons.

----------

## How To Use

### IFTTT 

    1) create a new applet
    2) in the 'this', search for webhooks
    3) select 'receive a web request', and give it a title, one word using hyphens or underscores if necessary
    4) finish the applet
    5) go to `https://ifttt.com/maker_webhooks` then click 'documentation'
    6) write down the web request url, and replace {event} with the title you gave in step 3
    7) in the web-trigger app settings, you will fill in the event URL with the URL from step 6

note: with IFTTT it might be easiest to:

* keep 'URL Prefix' blank
* turn off 'use common prefix'
* turn off 'url-encode messages'
* put a separate URL in each input specific message, if each input has a different {event}

### AutoRemote/Tasker

coming soon (upon request)

### Join/Tasker

coming soon (upon request)

----------

## Features:

* seven inputs that all send a different web request
* you can optionally use a common prefix for all actions (if all seven endpoints use the same IoT service)
* or you can specify a different full URL for each action.

----------

## Planned:

* tap and hold screen and buttons for more actions
* double-tap screen and buttons
* on-screen labels for inputs
