registerSettingsPage(settingsComponent);

function setDefaults(props: SettingsComponentProps) {
    if (!props.settings['use-prefix']) {
        props.settingsStorage.setItem('use-prefix', 'true');
    }

    if (!props.settings['message-encode']) {
        props.settingsStorage.setItem('message-encode', 'true');
    }
}

function settingsComponent(props: SettingsComponentProps) {
    setDefaults(props);

    return (
        <Page>
            <Section title={ <Text bold align='center'>Remote URL Trigger</Text> }>
                <Text>You can use this app to trigger some remote HTTP endpoints for IoT services, such as AutoRemote for Android/Tasker or IFTTT.</Text>
            </Section>

            <Section title={ <Text bold align='center'>Global Settings</Text> }>
                <Text italic>'URL Prefix' is where can enter everything from http(s):// until just before the message will be appended.</Text>
                <TextInput label='URL Prefix' placeholder='http://remote.control.com/path?message=' settingsKey='url-prefix' />
                <Text italic>Turn 'Use Common Prefix' <Text bold>off</Text> if you wish for each different input to trigger a different service.</Text>
                <Toggle label='Use Common Prefix' settingsKey='use-prefix' />
            </Section>

            <Section title={ <Text bold align='center'>Input Specific Messages</Text> }>
                <Text italic>These messages will be optionally URL-encoded and appended to the prefix above.</Text>
                <Toggle label='URL-Encode Messages' settingsKey='message-encode' />
                <TextInput label='Top Button Message' placeholder='fitbit=:=tap-top' settingsKey='tap-top' />
                <TextInput label='Bottom Button Message' placeholder='fitbit=:=tap-bottom' settingsKey='tap-bottom' />
                <TextInput label='Screen Tap Message' placeholder='fitbit=:=screen-tap' settingsKey='screen-tap' />
                <TextInput label='Swipe Up Message' placeholder='fitbit=:=swipe-up' settingsKey='swipe-up' />
                <TextInput label='Swipe Down Message' placeholder='fitbit=:=swipe-down' settingsKey='swipe-down' />
                <TextInput label='Swipe Left Message' placeholder='fitbit=:=swipe-left' settingsKey='swipe-left' />
                <TextInput label='Swipe Right Message' placeholder='fitbit=:=swipe-right' settingsKey='swipe-right' />
            </Section>
        </Page>
    );
}
