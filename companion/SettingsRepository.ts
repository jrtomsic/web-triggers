import { settingsStorage } from 'settings';

export class SettingsRepository {
    private static getSetting(name: string): string {
        const item = settingsStorage.getItem(name);

        if (item === null || item === undefined) {
            console.log(`'${name}' doesn't exist in settingsStorage`);
            return null;
        }

        return item;
    }

    public static getTextInputSetting(name: string): string {
        const item = this.getSetting(name);

        if (item === null) {
            return null;
        }

        let value = null;
        try {
            value = JSON.parse(item).name;
        } catch (ex) {
            console.log(`error parsing json from settingsStorage '${name}': ${item}`);
            return null;
        }

        if (value === '') {
            console.log(`value for '${name}' is blank in settingsStorage`);
            return null;
        }

        return value;
    }

    public static getToggleSetting(name: string): boolean {
        const item = this.getSetting(name);

        if (item === null) {
            return null;
        }

        return (item === 'true');
    }

    public static getNormalizedURLPrefix(): string {
        const urlPrefix = this.getTextInputSetting('url-prefix');
        const usePrefix = this.getToggleSetting('use-prefix');

        if (usePrefix === false || urlPrefix === null) {
            return '';
        }

        return urlPrefix;
    }

    public static isURLPrefixValid(): boolean {
        const urlPrefix = this.getTextInputSetting('url-prefix');
        const usePrefix = this.getToggleSetting('use-prefix');

        if (usePrefix === true && urlPrefix === null) {
            return false;
        }

        return true;
    }

    public static getNormalizedMessage(inputType: string): string {
        const message = this.getTextInputSetting(inputType);
        const encode = this.getToggleSetting('message-encode');

        if (message === null) {
            return null;
        }

        if (encode === true) {
            return encodeURIComponent(message);
        }

        return message;
    }
}
