import { MessageEvent, peerSocket } from 'messaging';
import { InputEnum } from '../common/InputEnum';
import { SettingsRepository } from './SettingsRepository';

export class MessageListener {
    public subscribe() {
        peerSocket.addEventListener('message', this.handleMessageEvent.bind(this));
    }

    private async handleMessageEvent(event: MessageEvent) {
        if ((event.data in InputEnum) === false) {
            return;
        }

        const endpoint = SettingsRepository.getNormalizedURLPrefix();
        const isEndpointValid = SettingsRepository.isURLPrefixValid();
        const message = SettingsRepository.getNormalizedMessage(event.data);

        if (isEndpointValid === false) {
            this.sendMessage('failure', 'invalid\nurl prefix\nsetting');
            return;
        }

        if (message === null) {
            this.sendMessage('failure', 'invalid\nmessage\nsetting');
            return;
        }

        try {
            await fetch(endpoint + message, { method: 'GET' });
        } catch (ex) {
            this.sendMessage('failure', 'endpoint\nrejected');
            return;
        }

        this.sendMessage('success', 'success');
        return;
    }

    private sendMessage(status: string, message: string) {
        peerSocket.send({ status, message });
    }
}
