import { ConnectionListener } from './ConnectionListener';
import { MessageListener } from './MessageListener';

const connectionListener = new ConnectionListener();
const messageListener = new MessageListener();

connectionListener.subscribe();
messageListener.subscribe();
