import { me } from 'companion';
import { MessageEvent, peerSocket } from 'messaging';

export class ConnectionListener {
    public subscribe() {
        peerSocket.addEventListener('message', this.handleMessageEvent.bind(this));
    }

    private handleMessageEvent(event: MessageEvent) {
        if (me.permissions.granted('access_internet') === false) {
            peerSocket.send({ status: 'no-internet' });
            return;
        }

        if (event.data === 'ping') {
            peerSocket.send({ status: 'pong' });
            return;
        }
    }
}
