export enum InputEnum {
    'tap-top' = 'tap-top',
    'tap-bottom' = 'tap-bottom',
    'swipe-up' = 'swipe-up',
    'swipe-down' = 'swipe-down',
    'swipe-left' = 'swipe-left',
    'swipe-right' = 'swipe-right',
    'screen-tap' = 'screen-tap',
}
