import document from 'document';
import { InputEnum } from '../common/InputEnum';
import { TriggerMessageHandler } from './TriggerMessageHandler';

type ValidButton = 'up' | 'down';
enum TapButtonMap {
    up = 'tap-top',
    down = 'tap-bottom',
}

export class ButtonListener {
    constructor(private readonly triggerMessageHandler: TriggerMessageHandler) {}

    public subscribe() {
        document.addEventListener('keypress', this.press.bind(this));
    }

    private press(event: KeyboardEvent) {
        const key = TapButtonMap[(event.key as ValidButton)];

        this.triggerMessageHandler.handleInputEvent(InputEnum[key]);
    }
}
