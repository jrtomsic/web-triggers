import { MessageEvent, peerSocket } from 'messaging';
import { FeedbackHandler } from './FeedbackHandler';

export class MessageListener {
    constructor (private readonly feedbackHandler: FeedbackHandler) {}

    public subscribe() {
        peerSocket.addEventListener('message', this.handleMessageEvent.bind(this));
    }

    private handleMessageEvent(event: MessageEvent) {
        if (event.data.status === 'success') {
            this.feedbackHandler.success(event.data.message);
        }

        if (event.data.status === 'failure') {
            this.feedbackHandler.failure(event.data.message);
        }
    }
}
