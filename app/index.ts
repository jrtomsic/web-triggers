import { ButtonListener } from './ButtonListener';
import { ConnectionListener } from './ConnectionListener';
import { FeedbackHandler } from './FeedbackHandler';
import { MessageListener } from './MessageListener';
import { SwipeListener } from './SwipeListener';
import { TriggerMessageHandler } from './TriggerMessageHandler';

const feedbackHandler = new FeedbackHandler();
const connectionListener = new ConnectionListener(feedbackHandler);
const triggerMessageHandler = new TriggerMessageHandler(feedbackHandler, connectionListener);
const messageListener = new MessageListener(feedbackHandler);
const screenListener = new SwipeListener(triggerMessageHandler);
const buttonListener = new ButtonListener(triggerMessageHandler);

screenListener.subscribe();
messageListener.subscribe();
buttonListener.subscribe();
connectionListener.subscribe();

connectionListener.waitForConnection();
