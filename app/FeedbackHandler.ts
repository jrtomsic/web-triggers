import document from 'document';
import { vibration } from 'haptics';

export class FeedbackHandler {
    private readonly spinner: GraphicsElement;
    private readonly background: GraphicsElement;
    private readonly status: GraphicsElement;
    private messageTimeout: number;
    private spinnerTimeout: number;

    constructor() {
        this.background = document.getElementById('background') as GraphicsElement;
        this.status = document.getElementById('status') as GraphicsElement;
        this.spinner = document.getElementById('spinner') as GraphicsElement;
    }

    public failure(message: string, reset: boolean = true): void {
        this.idle();

        vibration.start('nudge-max');
        this.background.style.fill = 'darkred';
        this.status.text = message;

        console.log(`${message.split('\n').join(' ')}`);

        if (reset) {
            clearTimeout(this.messageTimeout);
            this.messageTimeout = setTimeout(this.ready.bind(this), 3000);
        }
    }

    public success(message: string): void {
        this.idle();

        vibration.start('confirmation');
        this.background.style.fill = 'darkgreen';
        this.status.text = message;

        console.log(`${message.split('\n').join(' ')}`);

        clearTimeout(this.messageTimeout);
        this.messageTimeout = setTimeout(this.ready.bind(this), 3000);
    }

    public ready(): void {
        this.idle();
        this.background.style.fill = 'black';
        this.status.text = 'ready...';
    }

    public connecting(): void {
        this.background.style.fill = 'darkorange';
        this.status.text = 'connecting\nto phone';
    }

    public processing(): void {
        clearTimeout(this.spinnerTimeout);
        this.spinnerTimeout = setTimeout(() => { this.spinner.state = 'enabled'; }, 500);
    }

    public idle(): void {
        clearTimeout(this.spinnerTimeout);
        this.spinner.state = 'disabled';
    }
}
