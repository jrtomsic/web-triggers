import { peerSocket } from 'messaging';
import { InputEnum } from '../common/InputEnum';
import { ConnectionListener } from './ConnectionListener';
import { FeedbackHandler } from './FeedbackHandler';

export class TriggerMessageHandler {
    constructor (
        private readonly feedbackHandler: FeedbackHandler,
        private readonly connectionListener: ConnectionListener,
    ) {}

    public handleInputEvent(action: InputEnum) {
        if (this.connectionListener.isConnected() === false) {
            return;
        }

        this.feedbackHandler.processing();

        try {
            peerSocket.send(action);
        } catch (ex) {
            this.connectionListener.waitForConnection();
        }

        return;
    }
}
