import { MessageEvent, peerSocket } from 'messaging';
import { FeedbackHandler } from './FeedbackHandler';

const SECONDS_TIMEOUT = 30;

export class ConnectionListener {
    private connected: boolean;
    private connectionTimeout: number;

    constructor(private readonly feedbackHandler: FeedbackHandler) {
        this.connected = false;
    }

    public isConnected(): boolean {
        return this.connected;
    }

    public subscribe() {
        peerSocket.addEventListener('message', this.handleMessageEvent.bind(this));
        peerSocket.addEventListener('close', this.waitForConnection.bind(this));
        peerSocket.addEventListener('open', this.connectionEstablished.bind(this));
    }

    public waitForConnection() {
        this.connected = false;
        this.feedbackHandler.connecting();
        this.feedbackHandler.processing();

        this.connectionTimeout = setTimeout(this.connectionTimedOut.bind(this), SECONDS_TIMEOUT * 1000);
    }

    private connectionTimedOut() {
        clearTimeout(this.connectionTimeout);
        this.feedbackHandler.failure('connection\ntimeout', false);
    }

    private connectionEstablished(event: MessageEvent) {
        try {
            peerSocket.send('ping');
        } catch (ex) {
        }
    }

    private handleMessageEvent(event: MessageEvent) {
        if (event.data.status === 'pong') {
            clearTimeout(this.connectionTimeout);
            this.connected = true;
            this.feedbackHandler.ready();
        }

        if (event.data.status === 'no-internet') {
            clearTimeout(this.connectionTimeout);
            this.connected = false;
            this.feedbackHandler.failure('internet\npermission\ndenied', false);
        }
    }
}
