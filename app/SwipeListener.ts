import document from 'document';
import { InputEnum } from '../common/InputEnum';
import { TriggerMessageHandler } from './TriggerMessageHandler';

const SWIPE_THRESHOLD: number = 25;

export class SwipeListener {
    private startX: number;
    private startY: number;
    private pressed: boolean;
    private element: Element;

    constructor (private readonly triggerMessageHandler: TriggerMessageHandler) {
        this.startX = 0;
        this.startY = 0;
        this.pressed = false;
        this.element = document.getElementById('foreground');
    }

    public subscribe() {
        this.element.addEventListener('mousedown', this.press.bind(this));
        this.element.addEventListener('mouseup', this.release.bind(this));
    }

    private press(event: MouseEvent) {
        this.pressed = true;

        this.startX = event.screenX;
        this.startY = event.screenY;

        return;
    }

    private release(event: MouseEvent) {
        if (this.pressed !== true) {
            return;
        }

        const deltaX = event.screenX - this.startX;
        const deltaY = event.screenY - this.startY;
        this.pressed = false;

        if (Math.abs(deltaX) < SWIPE_THRESHOLD && Math.abs(deltaY) < SWIPE_THRESHOLD) {
            return this.triggerMessageHandler.handleInputEvent(InputEnum['screen-tap']);
        }

        if (Math.abs(deltaX) < Math.abs(deltaY)) {
            if (deltaY > 0) {
                return this.triggerMessageHandler.handleInputEvent(InputEnum['swipe-down']);
            }

            return this.triggerMessageHandler.handleInputEvent(InputEnum['swipe-up']);
        }

        if (deltaX > 0) {
            return this.triggerMessageHandler.handleInputEvent(InputEnum['swipe-right']);
        }

        return this.triggerMessageHandler.handleInputEvent(InputEnum['swipe-left']);
    }
}
